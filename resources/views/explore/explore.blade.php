@extends('layouts.app')

@section('content')
    <div>
        @foreach ($users as $user)
            <a href="{{ route('profile.show',$user->username) }}" class="flex items-center mb-5">
                <img src="{{ $user->getAvatar($user->avatar) }}" alt="" width="60px" height="60px" class="mr-4 rounded" >

                <div>
                    <h4 class="font-bold">{{ '@' . $user->username }}</h4>
                </div>
            </a>
        @endforeach
       <div class="flex"> {{ $users->links() }} </div>
    </div>
@endsection
