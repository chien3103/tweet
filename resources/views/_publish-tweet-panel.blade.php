<div class="border border-blue-400 rounded-lg px-8 py-6 mb-8">
    <form action="{{ route('tweet.store') }}" method="post">
        @csrf
        <textarea name="body" id="" class="w-full" placeholder="What's up doc?"></textarea>
        <hr class="my-4">
        <footer class="flex justify-between">
        <img src="{{ auth()->user()->getAvatar(auth()->user()->avatar) }}" alt="" class="rounded-full mr-2" height="50" width="50">
        <button type="submit" class="bg-blue-500 rounded-lg shadow py-2 px-2 text-white" >Publish</button>
        </footer>
    </form>
</div>
