<h3 class="font-bold text-xl mv-4">Following</h3>

<ul>
    @if (Auth::check())

    @forelse (auth()->user()->follows as $user)

    <li>
        <div class="flex items-center text-sm">
            <a href="{{ route('profile.show',$user->username) }}">
                <img src="{{ $user->getAvatar($user->avatar) }}" alt="" class="rounded-full mr-2" height="50" width="50">
            </a>
            <a href="{{ route('profile.show',$user->username) }}">
                {{ $user->name }}
            </a>
        </div>
    </li>
    @empty
    Not friend yet.
    @endforelse
    @endif
</ul>
