@extends('layouts.app')

@section('content')

    <header class="mb-10 relative">
        <img src="{{ asset('/images/anhbia2.jfif') }}"  alt="" width="100%"  >

        <div class="flex justify-between items-center" >
            <div>
                <h2 class="font-bold text-2xl mb-0">{{ $user->name }}</h2>
            </div>

            {{-- button --}}
            <div class="flex">
                @if (auth()->user()->is($user))
                <a href="{{ route('profile.edit',$user->username) }}" class="rounded-full border border-gray-300 shadow py-2 px-5 text-black text-xs mr-3">
                Edit Profile
                </a>
                @endif


                @if (auth()->user()->isNot($user))
                <form action="{{ route('profile.follow',$user->username) }}" method="POST">
                    @csrf
                    <button type="submit" class="bg-blue-500 rounded-full shadow py-2 px-5 text-white text-xs">
                       {{ auth()->user()->following($user)?'Unfollow Me':'Follow Me' }}

                    </button>
                </form>
                @endif
           </div>
        </div>
        <img src="{{ $user->getAvatar($user->avatar) }}" alt=""
            class="rounded-full mr-2 absolute"
            height="50"
            style="width: 150px;left: calc(50% - 80px);top: 200px"
            >
            <p class="text-sm " style="top:100px">
                Good descriptive writing creates an impression in the reader's mind of an event, a place, a person, or a thing.
                The writing will be such that it will set a mood or describe something in such detail that if the reader saw it, they would recognize it.
            </p>
    </header>

@include('_timeline',['tweets' => $user->tweets])

@endsection
