@extends('layouts.app')

@section('content')
{{-- {{ route('profile.update',$user->username) }} --}}
    <form action="{{ route('profile.update',$user->username) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method("PATCH")
        <div class="mb-6">
            <label for="name" class="block mb-2 uppercase font-bold text-xs text-gray-700">Name</label>
            <input type="text" name="name" class="border border-gray-400 p-2 w-full" value="{{ $user->name }}">
            @error('name')
                <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <label for="username" class="block mb-2 uppercase font-bold text-xs text-gray-700">Avatar</label>
            <input type="file" name="avatar" class="border border-gray-400 p-2 w-full">
            @error('avatar')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <label for="username" class="block mb-2 uppercase font-bold text-xs text-gray-700">Username</label>
            <input type="text" name="username" class="border border-gray-400 p-2 w-full" value="{{ $user->username }}">
            @error('username')
                <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <label for="email" class="block mb-2 uppercase font-bold text-xs text-gray-700">Email</label>
            <input type="text" name="email" class="border border-gray-400 p-2 w-full" value="{{ $user->email }}">
            @error('email')
                <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <label for="password" class="block mb-2 uppercase font-bold text-xs text-gray-700">Password</label>
            <input type="password" name="password" class="border border-gray-400 p-2 w-full">
            @error('password')
                <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <label for="password_confirmation" class="block mb-2 uppercase font-bold text-xs text-gray-700">Password Confirmation</label>
            <input type="password" name="password_confirmation" class="border border-gray-400 p-2 w-full">
            @error('password')
                <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <button type="submit" class="bg-blue-400 text-white rounded py-2 px-4 hover:bg-blue-500 mr-4">
                Submit
            </button>

            <a href="" class="hover:underline">Cancel</a>
        </div>
    </form>
@endsection
