<div class="flex p-4 border-b border-b-gray-400">

        <div class="mr-4 flex-shrink-0">
            <img src="{{ $tweet->user->getAvatar($tweet->user->avatar) }}" alt="" class="rounded-full mr-2" height="50" width="50">
        </div>
        <div>
            <h5 class="font-bold mb-4">{{ $tweet->user->name }}</h5>

            <p class="text-sm">
                {{ $tweet->body }}
            </p>
            <div class="flex">
                {{-- <form action="{{ route('like.store',$tweet->id) }}" method="post">
                    @csrf
                    <div class="flex items-center mr-4">
                    <button type="submit"><i style="font-size:24px " class="fa {{ $tweet->isLikeBy(auth()->user())?'text-blue-500' :'text-gray-500' }}">&#xf087;</i></button>
                    <span class="text-xs text-gray-500">
                        {{ $tweet->likes ?  : 0 }}
                    </span>
                    </div>
                </form> --}}

                <div class="flex items-center mr-4">
                <a href="javascript:" onclick="like({{ $tweet->id }})">
                    <i style="font-size:24px " class="fa like {{ $tweet->isLikeBy(auth()->user())?'text-blue-500' :'text-gray-500' }}">&#xf087;</i>
                </a>
                    <span class="text-xs text-gray-500 ">
                        {{ $tweet->likes ?  : 0 }}
                    </span>
                </div>


                {{-- <form action="{{ route('like.destroy',$tweet->id) }}" method="post">
                    @csrf
                    @method('PATCH')
                    <div class="flex items-center mr-4">
                    <button type="submit"><i style="font-size:24px" class="fa {{ $tweet->isDislikeBy(auth()->user())?'text-blue-500' :'text-gray-500' }}">&#xf088;</i></button>
                    <span class="text-xs text-gray-500">

                        {{ $tweet->dislikes?  :0 }}
                    </span>
                    </div>
                </form> --}}
                <div class="flex items-center mr-4">
                    <a href="javascript:" onclick="dislike({{ $tweet->id }})">
                        <i style="font-size:24px" class="fa {{ $tweet->isDislikeBy(auth()->user())?'text-blue-500' :'text-gray-500' }}">&#xf088;</i>
                    </a>
                        <span class="text-xs text-gray-500 ">
                            {{ $tweet->dislikes ?  : 0 }}
                        </span>
                    </div>
            </div>
        </div>
</div>
<script>
    function like(id){
        $.ajax({
            type: "GET",
            url: "/tweet/" + id + "/like",
            // data: "data",
            // dataType: "dataType",
            success: function (response) {
                location.reload();
            }
        });
    }

    function dislike(id){
        $.ajax({
            type: "GET",
            url: "/tweet/" + id + "/dislike",
            // data: "data",
            // dataType: "dataType",
            success: function (response) {
                location.reload();
            }
        });
    }
</script>
