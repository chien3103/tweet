<ul>
    <li>
        <a href="{{ route('home') }}" class="font-bold text-lg mb-4 block">Home</a>
    </li>
    <li>
        <a href="{{ route('explore') }}" class="font-bold text-lg mb-4 block">Explore</a>
    </li>

    @if (Auth::check())

    <li>
        <a href="{{ route('profile.show',auth()->user()->username) }}" class="font-bold text-lg mb-4 block">Profile</a>
    </li>
    @endif
    <li>
        <form action="{{ route('logout') }}" method="post">
            @csrf
            <button class="font-bold text-lg mb-4 block">Logout</button>
        </form>
    </li>
</ul>
