<?php

namespace App;

use App\Models\Tweet;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getAvatar($value)
    {
        return asset($this->avatar?'/storage/'.$value : '/images/defaulte.jfif');
        //return asset( $this->avatar ?'/storage/'.$value:'/image/avatar.png');
    }

    public function timeline()
    {
        // $tweet = Tweet::all();
        // dd($tweet->countLikeDislike());
        $friends = $this->follows()->pluck('id');
        return Tweet::whereIn('user_id',$friends)
                ->orWhere('user_id', auth()->id())
                ->WithLikes()
                ->latest()
                ->get();
    }

    //tao chuc nang follow
    public function follow(User $user)
    {
        return $this->follows()->save($user);
    }
    public function unfollow(User $user)
    {
        return $this->follows()->detach($user);
    }
    //da theo doi
    public function following(User $user)
    {
        return $this->follows()->where('following_user_id',$user->id)->exists();
    }
    public function toggleFollow(User $user)
    {
        if($this->following($user)){
            return $this->unfollow($user);
        }

        return $this->follow($user);
    }
    public function follows()
    {
        return $this->belongsToMany('App\User', 'follows', 'user_id', 'following_user_id');
    }

    public function tweets()
    {
        return $this->hasMany(Tweet::class);
    }
}
