<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    protected $guarded = [];

    public function scopeWithLikes(Builder $query)
    {
        return $query->leftJoinSub(
            'SELECT tweet_id, SUM(liked) as likes, SUM(!liked) as dislikes from likes GROUP BY tweet_id',
            'likes',
            'tweets.id', 'likes.tweet_id'
        );
    }

    public function isLikeBy(User $user)
    {
        return $this->likes()->where('user_id',$user->id)->where('liked',true)->exists();
    }

    public function isDislikeBy(User $user)
    {
        return $this->likes()->where('user_id',$user->id)->where('liked',false)->exists();
    }

    public function dislike($user = null)
    {
        return $this->like($user,false);
    }

    public function like($user = null,$liked = true)
    {
        return $this->likes()->updateOrCreate(
           ['user_id' => $user? $user->id : auth()->id()],
           ['liked' => $liked]
       );
    }

    public function likes()
    {
        return $this->hasMany('App\Models\Like');
    }

    public function addTweet($body)
    {
        return Tweet::create([
            'user_id' => auth()->user()->id,
            'body' => $body
        ]);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
