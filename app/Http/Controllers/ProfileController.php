<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function update(User $user)
    {
        $attributes = request()->validate([
            //Rule::unique('users')->ignore($user)
            'name' => 'required|string|max:255',
            'username' => ['required','string','max:255','alpha_dash',Rule::unique('users')->ignore($user)],
            'email' => ['required','string','email','max:255', Rule::unique('users')->ignore($user)],
            'password' => 'string|required|min:8|max:255|confirmed',
            'avatar' => ['image']
        ]);
        if(request('avatar')){
            $attributes['avatar'] = request('avatar')->store('avatars');
        }
        $attributes['password'] = Hash::make(request('password'));
        $user->update($attributes);

        return redirect()->route('profile.show',$user->username);
    }
    public function show(User $user)
    {
        // $this->authorize('edit',$user);
        return view('profile.show',['user'=>$user]);
    }
    public function edit(User $user)
    {
        return view('profile.edit',['user'=>$user]);
    }
}
