<?php

namespace App\Http\Controllers;

use App\Models\Tweet;
use Illuminate\Http\Request;

class TweetController extends Controller
{
    public function store(Tweet $tweet)
    {
        $attribute = request()->validate([
            'body' => 'required',
        ]);

        $tweet->addTweet(request()->body);

        return back();
    }
    public function index()
    {
        $tweets =  Tweet::all();
        return view('tweet.index',['tweets'=>auth()->user()->timeline()]);
    }
}
