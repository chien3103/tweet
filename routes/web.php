<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/tweets', 'TweetController@index')->name('home');
    Route::post('/tweets', 'TweetController@store')->name('tweet.store');

    Route::get('/profile/{user:username}/edit', 'ProfileController@edit')->name('profile.edit');
    Route::patch('/profile/{user:username}', 'ProfileController@update')->name('profile.update');

    Route::post('/follow/{user:username}','FollowController@follow')->name('profile.follow');

    Route::get('/tweet/{tweet}/like','LikeTweetController@store')->name('like.store');
    Route::get('/tweet/{tweet}/dislike','LikeTweetController@destroy')->name('like.destroy');
});



Route::get('/profile/{user:username}','ProfileController@show')->name('profile.show');
Route::get('/explore','ExploreController@explore')->name('explore');

